#include "Node.h"

void Node::insert(int val)
{
    if (val >= this->val)
    {
        if (!right) right = new Node(val, this);
        else right->insert(val);
    }
    else
    {
        if (!left) left = new Node(val, this);
        else left->insert(val);
    }
}


Node* Node::min()
{
    if (!left) return this;
    else return left->min();
}


Node* Node::find(int val)
{
    if (val == this->val) return this;
    if (val >= this->val)
    {
        if (!right) return nullptr;
        else right->find(val);
    }
    else
    {
        if (!left) return nullptr;
        else left->find(val);
    }
}


void Node::remove(int val)
{
    Node* rm_node = find(val);
    if (!rm_node->right && !rm_node->left)
    {
        if (rm_node->parent->left == rm_node)
            rm_node->parent->left = nullptr;
        else
            rm_node->parent->right = nullptr;
        delete rm_node;
    }
    else if (rm_node->right && rm_node->left)
    {
        Node* removable =  rm_node->right->min();
        int temp_val = removable->val;
        remove(temp_val);
        rm_node->val = temp_val;
    }
    else
    {
        Node* new_child = rm_node->left ? rm_node->left : rm_node->right;
        new_child->parent = rm_node->parent;
        if (rm_node->val < rm_node->parent->val)
            rm_node->parent->left = new_child;
        else
            rm_node->parent->right = new_child;
        delete rm_node;
    }
}

