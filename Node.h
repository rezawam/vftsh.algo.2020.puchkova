#pragma once

typedef class Node
{
private:
    int val = 0;
    Node* left = nullptr;
    Node* right = nullptr;
    Node* parent = nullptr;

public:
    Node (int val) : val(val) {};
    Node (int val, Node* parent) : val(val), parent(parent) {};

    int value() { return val; }
    Node* get_parent() { return parent; }
    Node* L() { return left; }
    Node* R() { return right; }

    void insert(int val);
    void remove(int val);
    Node* find(int val);
    Node* min();
} Node;

