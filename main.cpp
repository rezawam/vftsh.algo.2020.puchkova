#include <cstdio>
#include "Node.h"

int main ()
{
    // Example
    Node myBST(50);
    myBST.insert(30);
    myBST.insert(70);
    myBST.insert(60);
    myBST.insert(80);
    myBST.insert(20);
    myBST.insert(40);
    /*
             50
           /     \
          30      70
         /  \    /  \
       20   40  60   80

    */
    myBST.remove(20);
    myBST.remove(30);
    myBST.remove(50);
    /*
         60
       /    \
      40    70
              \
               80
    */
    printf("%d\n", myBST.value()); // print 60 as root
    printf("%d\n", myBST.L()->value()); // print 40
    printf("%d\n", myBST.R()->value()); // print 70
    printf("%d\n", myBST.find(80)->get_parent()->value()); // print 70

}

